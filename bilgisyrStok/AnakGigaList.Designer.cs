﻿namespace bilgisyrStok
{
    partial class AnakGigaList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Gigabtnlist = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gigageri = new System.Windows.Forms.Button();
            this.artalGiga = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Gigabtnlist
            // 
            this.Gigabtnlist.Location = new System.Drawing.Point(188, 159);
            this.Gigabtnlist.Name = "Gigabtnlist";
            this.Gigabtnlist.Size = new System.Drawing.Size(75, 23);
            this.Gigabtnlist.TabIndex = 0;
            this.Gigabtnlist.Text = "Listele";
            this.Gigabtnlist.UseVisualStyleBackColor = true;
            this.Gigabtnlist.Click += new System.EventHandler(this.Gigabtnlist_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(23, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(240, 150);
            this.dataGridView1.TabIndex = 1;
            // 
            // gigageri
            // 
            this.gigageri.Location = new System.Drawing.Point(23, 159);
            this.gigageri.Name = "gigageri";
            this.gigageri.Size = new System.Drawing.Size(75, 23);
            this.gigageri.TabIndex = 2;
            this.gigageri.Text = "Geri";
            this.gigageri.UseVisualStyleBackColor = true;
            this.gigageri.Click += new System.EventHandler(this.gigageri_Click);
            // 
            // artalGiga
            // 
            this.artalGiga.Location = new System.Drawing.Point(188, 198);
            this.artalGiga.Name = "artalGiga";
            this.artalGiga.Size = new System.Drawing.Size(75, 23);
            this.artalGiga.TabIndex = 3;
            this.artalGiga.Text = "Artan Fiyat";
            this.artalGiga.UseVisualStyleBackColor = true;
            this.artalGiga.Click += new System.EventHandler(this.artalGiga_Click);
            // 
            // AnakGigaList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.artalGiga);
            this.Controls.Add(this.gigageri);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.Gigabtnlist);
            this.Name = "AnakGigaList";
            this.Text = "AnakGigaList";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Gigabtnlist;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button gigageri;
        private System.Windows.Forms.Button artalGiga;
    }
}