﻿namespace bilgisyrStok
{
    partial class EkMsı
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataEkMsı = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.ArtanEkmsı = new System.Windows.Forms.Button();
            this.Ekmsıgeri = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataEkMsı)).BeginInit();
            this.SuspendLayout();
            // 
            // dataEkMsı
            // 
            this.dataEkMsı.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataEkMsı.Location = new System.Drawing.Point(14, 12);
            this.dataEkMsı.Name = "dataEkMsı";
            this.dataEkMsı.Size = new System.Drawing.Size(258, 156);
            this.dataEkMsı.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(202, 178);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(70, 24);
            this.button1.TabIndex = 1;
            this.button1.Text = "listele";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ArtanEkmsı
            // 
            this.ArtanEkmsı.Location = new System.Drawing.Point(202, 208);
            this.ArtanEkmsı.Name = "ArtanEkmsı";
            this.ArtanEkmsı.Size = new System.Drawing.Size(75, 23);
            this.ArtanEkmsı.TabIndex = 2;
            this.ArtanEkmsı.Text = "Artan Fiyat";
            this.ArtanEkmsı.UseVisualStyleBackColor = true;
            this.ArtanEkmsı.Click += new System.EventHandler(this.ArtanEkmsı_Click);
            // 
            // Ekmsıgeri
            // 
            this.Ekmsıgeri.Location = new System.Drawing.Point(14, 179);
            this.Ekmsıgeri.Name = "Ekmsıgeri";
            this.Ekmsıgeri.Size = new System.Drawing.Size(75, 23);
            this.Ekmsıgeri.TabIndex = 3;
            this.Ekmsıgeri.Text = "Geri";
            this.Ekmsıgeri.UseVisualStyleBackColor = true;
            this.Ekmsıgeri.Click += new System.EventHandler(this.Ekmsıgeri_Click);
            // 
            // EkMsı
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Ekmsıgeri);
            this.Controls.Add(this.ArtanEkmsı);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataEkMsı);
            this.Name = "EkMsı";
            this.Text = "Msı";
            ((System.ComponentModel.ISupportInitialize)(this.dataEkMsı)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataEkMsı;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ArtanEkmsı;
        private System.Windows.Forms.Button Ekmsıgeri;
    }
}