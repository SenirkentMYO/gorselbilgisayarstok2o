﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class EkranForm : Form
    {
        public EkranForm()
        {
            InitializeComponent();
        }

        private void EkranForm_Load(object sender, EventArgs e)
        {
            comboekran.Items.Add("Asus");
            comboekran.Items.Add("MSI");
            comboekran.ForeColor = Color.DarkRed;
            comboekran.BackColor = Color.Gray;

        }

        private void btnEkran_Click(object sender, EventArgs e)
        {
            List<Donanim> don = new List<Donanim>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.Donanim.Where(c => c.ParcaAdi == comboekran.Text).ToList();
            if (comboekran.SelectedIndex == 0)
            {

              

                EkAsus girisform = new EkAsus();

                girisform.Show();
               
              
                this.Hide();
               
                
            }
            else if (comboekran.SelectedIndex == 1)
            {

                EkMsı girisform = new EkMsı();

                girisform.Show();

                this.Hide();
            }
                
            else if (comboekran.SelectedIndex == 2)
            {
            }
        }

        private void geriparca_Click(object sender, EventArgs e)
        {
            DonanımEntities dnm = new DonanımEntities();

            Form2 girisform = new Form2();

            girisform.Show();

            this.Hide();
        }
    }
}
