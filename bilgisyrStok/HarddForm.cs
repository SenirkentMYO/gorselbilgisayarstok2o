﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class HarddForm : Form
    {
        public HarddForm()
        {
            InitializeComponent();
        }

        private void HarddForm_Load(object sender, EventArgs e)
        {
            combohard.Items.Add("IBM");
            combohard.Items.Add("Toshiba");
            combohard.Items.Add("SeaGate");
            combohard.Items.Add("WD");
            combohard.ForeColor = Color.DarkRed;
            combohard.BackColor = Color.Gray;
                
        }

        private void btnhard_Click(object sender, EventArgs e)
        {
            List<Donanim> don = new List<Donanim>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.Donanim.Where(c => c.ParcaAdi == combohard.Text).ToList();
            if (combohard.SelectedIndex == 0)
            {



                HarrdiskIbmlist girisform = new HarrdiskIbmlist();

                girisform.Show();


                this.Hide();


            }
            else if (combohard.SelectedIndex == 1)
            {

                HardTosList girisform = new HardTosList();

                girisform.Show();

                this.Hide();
            }
            else if (combohard.SelectedIndex == 2)
            {

                HarddSegateList girisform = new HarddSegateList();

                girisform.Show();

                this.Hide();
            }else if (combohard.SelectedIndex == 3)
            {



                HardWDList girisform = new HardWDList();

                girisform.Show();


                this.Hide();


            }

        }

        private void parcageri_Click(object sender, EventArgs e)
        {
            DonanımEntities dnm = new DonanımEntities();



            Form2 girisform = new Form2();

            girisform.Show();

            this.Hide();
        }  
    }

}
