//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace bilgisyrStok
{
    using System;
    using System.Collections.Generic;
    
    public partial class WvHardDisk
    {
        public int HddID { get; set; }
        public string HddMarka { get; set; }
        public string HddModeli { get; set; }
        public string HddHızı { get; set; }
        public string HddKapasite { get; set; }
        public string HddBellek { get; set; }
        public string HddFiyat { get; set; }
    }
}
