﻿namespace bilgisyrStok
{
    partial class HarddForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combohard = new System.Windows.Forms.ComboBox();
            this.lblhard = new System.Windows.Forms.Label();
            this.btnhard = new System.Windows.Forms.Button();
            this.parcageri = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // combohard
            // 
            this.combohard.FormattingEnabled = true;
            this.combohard.Location = new System.Drawing.Point(141, 67);
            this.combohard.Name = "combohard";
            this.combohard.Size = new System.Drawing.Size(121, 21);
            this.combohard.TabIndex = 0;
            // 
            // lblhard
            // 
            this.lblhard.AutoSize = true;
            this.lblhard.ForeColor = System.Drawing.Color.OrangeRed;
            this.lblhard.Location = new System.Drawing.Point(69, 71);
            this.lblhard.Name = "lblhard";
            this.lblhard.Size = new System.Drawing.Size(57, 13);
            this.lblhard.TabIndex = 1;
            this.lblhard.Text = "Markalar =";
            // 
            // btnhard
            // 
            this.btnhard.Location = new System.Drawing.Point(187, 108);
            this.btnhard.Name = "btnhard";
            this.btnhard.Size = new System.Drawing.Size(75, 23);
            this.btnhard.TabIndex = 2;
            this.btnhard.Text = "Seç";
            this.btnhard.UseVisualStyleBackColor = true;
            this.btnhard.Click += new System.EventHandler(this.btnhard_Click);
            // 
            // parcageri
            // 
            this.parcageri.Location = new System.Drawing.Point(51, 108);
            this.parcageri.Name = "parcageri";
            this.parcageri.Size = new System.Drawing.Size(75, 23);
            this.parcageri.TabIndex = 3;
            this.parcageri.Text = "Geri";
            this.parcageri.UseVisualStyleBackColor = true;
            this.parcageri.Click += new System.EventHandler(this.parcageri_Click);
            // 
            // HarddForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.parcageri);
            this.Controls.Add(this.btnhard);
            this.Controls.Add(this.lblhard);
            this.Controls.Add(this.combohard);
            this.Name = "HarddForm";
            this.Text = "HardDisk";
            this.Load += new System.EventHandler(this.HarddForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox combohard;
        private System.Windows.Forms.Label lblhard;
        private System.Windows.Forms.Button btnhard;
        private System.Windows.Forms.Button parcageri;
    }
}