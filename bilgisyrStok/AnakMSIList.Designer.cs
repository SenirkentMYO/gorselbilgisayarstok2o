﻿namespace bilgisyrStok
{
    partial class AnakMSIList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.anaMsılist = new System.Windows.Forms.Button();
            this.dataMSIana = new System.Windows.Forms.DataGridView();
            this.msılistgeri = new System.Windows.Forms.Button();
            this.artanfiyat = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataMSIana)).BeginInit();
            this.SuspendLayout();
            // 
            // anaMsılist
            // 
            this.anaMsılist.Location = new System.Drawing.Point(197, 159);
            this.anaMsılist.Name = "anaMsılist";
            this.anaMsılist.Size = new System.Drawing.Size(75, 23);
            this.anaMsılist.TabIndex = 0;
            this.anaMsılist.Text = "Listele";
            this.anaMsılist.UseVisualStyleBackColor = true;
            this.anaMsılist.Click += new System.EventHandler(this.anaMsılist_Click);
            // 
            // dataMSIana
            // 
            this.dataMSIana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataMSIana.Location = new System.Drawing.Point(22, 3);
            this.dataMSIana.Name = "dataMSIana";
            this.dataMSIana.Size = new System.Drawing.Size(240, 150);
            this.dataMSIana.TabIndex = 1;
            // 
            // msılistgeri
            // 
            this.msılistgeri.Location = new System.Drawing.Point(22, 159);
            this.msılistgeri.Name = "msılistgeri";
            this.msılistgeri.Size = new System.Drawing.Size(75, 23);
            this.msılistgeri.TabIndex = 2;
            this.msılistgeri.Text = "Geri";
            this.msılistgeri.UseVisualStyleBackColor = true;
            this.msılistgeri.Click += new System.EventHandler(this.msılistgeri_Click);
            // 
            // artanfiyat
            // 
            this.artanfiyat.Location = new System.Drawing.Point(197, 188);
            this.artanfiyat.Name = "artanfiyat";
            this.artanfiyat.Size = new System.Drawing.Size(75, 23);
            this.artanfiyat.TabIndex = 3;
            this.artanfiyat.Text = "Artan Fiyat";
            this.artanfiyat.UseVisualStyleBackColor = true;
            this.artanfiyat.Click += new System.EventHandler(this.artanfiyat_Click);
            // 
            // AnakMSIList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.artanfiyat);
            this.Controls.Add(this.msılistgeri);
            this.Controls.Add(this.dataMSIana);
            this.Controls.Add(this.anaMsılist);
            this.Name = "AnakMSIList";
            this.Text = "AnakMSIList";
            ((System.ComponentModel.ISupportInitialize)(this.dataMSIana)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button anaMsılist;
        private System.Windows.Forms.DataGridView dataMSIana;
        private System.Windows.Forms.Button msılistgeri;
        private System.Windows.Forms.Button artanfiyat;
    }
}