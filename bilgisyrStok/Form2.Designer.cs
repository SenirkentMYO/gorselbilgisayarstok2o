﻿namespace bilgisyrStok
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbo1 = new System.Windows.Forms.ComboBox();
            this.lbl3 = new System.Windows.Forms.Label();
            this.btn2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbo1
            // 
            this.cmbo1.FormattingEnabled = true;
            this.cmbo1.Location = new System.Drawing.Point(98, 79);
            this.cmbo1.Name = "cmbo1";
            this.cmbo1.Size = new System.Drawing.Size(121, 21);
            this.cmbo1.TabIndex = 0;
            this.cmbo1.SelectedIndexChanged += new System.EventHandler(this.cmbo1_SelectedIndexChanged);
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl3.Location = new System.Drawing.Point(37, 82);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(56, 13);
            this.lbl3.TabIndex = 1;
            this.lbl3.Text = "ParcaAdı :";
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(121, 121);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 23);
            this.btn2.TabIndex = 2;
            this.btn2.Text = "Seç";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.lbl3);
            this.Controls.Add(this.cmbo1);
            this.Name = "Form2";
            this.Text = "ParcaAdlari";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbo1;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Button btn2;
    }
}