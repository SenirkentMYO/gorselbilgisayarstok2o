﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class AnakMSIList : Form
    {
        public AnakMSIList()
        {
            InitializeComponent();
        }

        private void anaMsılist_Click(object sender, EventArgs e)
        {
            List<WvAnaKart> don = new List<WvAnaKart>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvAnaKart.Where(a => a.AnaKMarka.Contains("MSI")).ToList();

            dataMSIana.DataSource = don;
        }

        private void msılistgeri_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            Anakform goster = new Anakform();
            goster.Show();
            this.Hide();

        }

        private void artanfiyat_Click(object sender, EventArgs e){
        
            DonanımEntities db = new DonanımEntities();
            List<WvAnaKart> listele = db.WvAnaKart.OrderBy(a => a.AnaKFiyati).Where(a => a.AnaKMarka.Contains("MSI")).ToList();
          
            dataMSIana.DataSource = listele;
        }
    }
}
