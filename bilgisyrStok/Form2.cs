﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {


              cmbo1.Items.Add("Ana Kart");
           
            cmbo1.Items.Add("Ekran Kartı");
            cmbo1.Items.Add("Hard Disk");
            cmbo1.Items.Add("İşlemci");
            cmbo1.ForeColor = Color.DarkRed;
            cmbo1.BackColor = Color.Gray;

            
        }

        private void btn2_Click(object sender, EventArgs e)
        {
             List<Donanim> don = new List<Donanim>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.Donanim.Where(c => c.ParcaAdi == cmbo1.Text).ToList();
            if(cmbo1.SelectedIndex==0){


            
                Anakform girisform = new Anakform();

                girisform.Show();

                this.Hide();

            }
            else if(cmbo1.SelectedIndex==1){
                
               EkranForm girisform = new EkranForm();

                girisform.Show();

                this.Hide();
            }
           else if (cmbo1.SelectedIndex == 2)
            {



                HarddForm girisform = new HarddForm();

                girisform.Show();

                this.Hide();

            }
            else if (cmbo1.SelectedIndex == 3)
            {



                İslemciForm girisform = new İslemciForm();

                girisform.Show();

                this.Hide();

            }
        }

        private void cmbo1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
