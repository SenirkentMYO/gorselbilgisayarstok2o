﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class EkMsı : Form
    {
        public EkMsı()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<WvEkranKarti> don = new List<WvEkranKarti>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvEkranKarti.Where(a => a.EKartMarka.Contains("MSI")).ToList();

            dataEkMsı.DataSource = don;
        }

        private void Ekmsıgeri_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();

            EkranForm goster = new EkranForm();
            goster.Show();
            this.Hide();
        }

        private void ArtanEkmsı_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            List<WvEkranKarti> listele = db.WvEkranKarti.OrderBy(a => a.EKartFiyat).Where(a => a.EKartMarka.Contains("MSI")).ToList();

            dataEkMsı.DataSource = listele;
        }
    }
}
