﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class Anakform : Form
    {
        public Anakform()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            List<Donanim> don = new List<Donanim>();
            DonanımEntities donentiti = new DonanımEntities();

            cmbomrk.Items.Add("Asus");
            cmbomrk.Items.Add("Msi");
            cmbomrk.Items.Add("Gigabyte");
            cmbomrk.BackColor = Color.Gray;
            cmbomrk.ForeColor = Color.DarkRed;
           

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<WvAnaKart> don = new List<WvAnaKart>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvAnaKart.Where(c => c.AnaKMarka == cmbomrk.Text).ToList();
            if (cmbomrk.SelectedIndex == 0)
            {



                AnAsusList girisform = new AnAsusList();

                girisform.Show();


                this.Hide();


            }
            else if (cmbomrk.SelectedIndex == 1)
            {

                AnakMSIList girisform = new AnakMSIList();

                girisform.Show();

                this.Hide();
            }

            else if (cmbomrk.SelectedIndex == 2)
            {
                AnakGigaList girisform = new AnakGigaList();

                girisform.Show();

                this.Hide();
            }
        }

        private void geri_Click(object sender, EventArgs e)
        {
            DonanımEntities dnm = new DonanımEntities();

            Form2 girisform = new Form2();

            girisform.Show();

            this.Hide();
        }
    }
}
