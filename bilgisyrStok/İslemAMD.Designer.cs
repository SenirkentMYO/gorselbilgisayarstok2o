﻿namespace bilgisyrStok
{
    partial class İslemAMD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataAmd = new System.Windows.Forms.DataGridView();
            this.ListeAmd = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataAmd)).BeginInit();
            this.SuspendLayout();
            // 
            // dataAmd
            // 
            this.dataAmd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataAmd.Location = new System.Drawing.Point(23, 12);
            this.dataAmd.Name = "dataAmd";
            this.dataAmd.Size = new System.Drawing.Size(240, 150);
            this.dataAmd.TabIndex = 0;
            // 
            // ListeAmd
            // 
            this.ListeAmd.Location = new System.Drawing.Point(206, 168);
            this.ListeAmd.Name = "ListeAmd";
            this.ListeAmd.Size = new System.Drawing.Size(57, 23);
            this.ListeAmd.TabIndex = 1;
            this.ListeAmd.Text = "Listele";
            this.ListeAmd.UseVisualStyleBackColor = true;
            this.ListeAmd.Click += new System.EventHandler(this.ListeAmd_Click);
            // 
            // İslemAMD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.ListeAmd);
            this.Controls.Add(this.dataAmd);
            this.Name = "İslemAMD";
            this.Text = "İslemAMD";
            ((System.ComponentModel.ISupportInitialize)(this.dataAmd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataAmd;
        private System.Windows.Forms.Button ListeAmd;
    }
}