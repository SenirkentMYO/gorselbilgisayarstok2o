﻿namespace bilgisyrStok
{
    partial class EkAsus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataw = new System.Windows.Forms.DataGridView();
            this.btnasus = new System.Windows.Forms.Button();
            this.artanEkasus = new System.Windows.Forms.Button();
            this.Ekasusgeri = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataw)).BeginInit();
            this.SuspendLayout();
            // 
            // dataw
            // 
            this.dataw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataw.Location = new System.Drawing.Point(12, 3);
            this.dataw.Name = "dataw";
            this.dataw.Size = new System.Drawing.Size(260, 136);
            this.dataw.TabIndex = 0;
            this.dataw.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataw_CellContentClick);
            // 
            // btnasus
            // 
            this.btnasus.Location = new System.Drawing.Point(197, 145);
            this.btnasus.Name = "btnasus";
            this.btnasus.Size = new System.Drawing.Size(75, 23);
            this.btnasus.TabIndex = 1;
            this.btnasus.Text = "Listele";
            this.btnasus.UseVisualStyleBackColor = true;
            this.btnasus.Click += new System.EventHandler(this.btnasus_Click);
            // 
            // artanEkasus
            // 
            this.artanEkasus.Location = new System.Drawing.Point(197, 174);
            this.artanEkasus.Name = "artanEkasus";
            this.artanEkasus.Size = new System.Drawing.Size(75, 23);
            this.artanEkasus.TabIndex = 2;
            this.artanEkasus.Text = "Artan Fiyat";
            this.artanEkasus.UseVisualStyleBackColor = true;
            this.artanEkasus.Click += new System.EventHandler(this.artanEkasus_Click);
            // 
            // Ekasusgeri
            // 
            this.Ekasusgeri.Location = new System.Drawing.Point(13, 144);
            this.Ekasusgeri.Name = "Ekasusgeri";
            this.Ekasusgeri.Size = new System.Drawing.Size(75, 23);
            this.Ekasusgeri.TabIndex = 3;
            this.Ekasusgeri.Text = "Geri";
            this.Ekasusgeri.UseVisualStyleBackColor = true;
            this.Ekasusgeri.Click += new System.EventHandler(this.Ekasusgeri_Click);
            // 
            // EkAsus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.Ekasusgeri);
            this.Controls.Add(this.artanEkasus);
            this.Controls.Add(this.btnasus);
            this.Controls.Add(this.dataw);
            this.Name = "EkAsus";
            this.Text = "EkAsus";
            ((System.ComponentModel.ISupportInitialize)(this.dataw)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataw;
        private System.Windows.Forms.Button btnasus;
        private System.Windows.Forms.Button artanEkasus;
        private System.Windows.Forms.Button Ekasusgeri;
    }
}