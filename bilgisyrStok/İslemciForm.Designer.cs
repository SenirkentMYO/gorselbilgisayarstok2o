﻿namespace bilgisyrStok
{
    partial class İslemciForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboislem = new System.Windows.Forms.ComboBox();
            this.lblislemci = new System.Windows.Forms.Label();
            this.Btnislemci = new System.Windows.Forms.Button();
            this.geriIslem = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboislem
            // 
            this.comboislem.FormattingEnabled = true;
            this.comboislem.Location = new System.Drawing.Point(133, 75);
            this.comboislem.Name = "comboislem";
            this.comboislem.Size = new System.Drawing.Size(121, 21);
            this.comboislem.TabIndex = 0;
            this.comboislem.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lblislemci
            // 
            this.lblislemci.AutoSize = true;
            this.lblislemci.ForeColor = System.Drawing.Color.Coral;
            this.lblislemci.Location = new System.Drawing.Point(62, 78);
            this.lblislemci.Name = "lblislemci";
            this.lblislemci.Size = new System.Drawing.Size(54, 13);
            this.lblislemci.TabIndex = 1;
            this.lblislemci.Text = "Markalar=";
            // 
            // Btnislemci
            // 
            this.Btnislemci.Location = new System.Drawing.Point(179, 122);
            this.Btnislemci.Name = "Btnislemci";
            this.Btnislemci.Size = new System.Drawing.Size(75, 23);
            this.Btnislemci.TabIndex = 2;
            this.Btnislemci.Text = "Seç";
            this.Btnislemci.UseVisualStyleBackColor = true;
            this.Btnislemci.Click += new System.EventHandler(this.Btnislemci_Click);
            // 
            // geriIslem
            // 
            this.geriIslem.Location = new System.Drawing.Point(52, 122);
            this.geriIslem.Name = "geriIslem";
            this.geriIslem.Size = new System.Drawing.Size(75, 23);
            this.geriIslem.TabIndex = 3;
            this.geriIslem.Text = "Geri";
            this.geriIslem.UseVisualStyleBackColor = true;
            this.geriIslem.Click += new System.EventHandler(this.geriIslem_Click);
            // 
            // İslemciForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.geriIslem);
            this.Controls.Add(this.Btnislemci);
            this.Controls.Add(this.lblislemci);
            this.Controls.Add(this.comboislem);
            this.Name = "İslemciForm";
            this.Text = "İslemciForm";
            this.Load += new System.EventHandler(this.İslemciForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboislem;
        private System.Windows.Forms.Label lblislemci;
        private System.Windows.Forms.Button Btnislemci;
        private System.Windows.Forms.Button geriIslem;
    }
}