﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class İslemciForm : Form
    {
        public İslemciForm()
        {
            InitializeComponent();
        }

        private void Btnislemci_Click(object sender, EventArgs e)
        {
            List<Donanim> don = new List<Donanim>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.Donanim.Where(c => c.ParcaAdi == comboislem.Text).ToList();
            if (comboislem.SelectedIndex == 0)
            {



                İslemAMD girisform = new İslemAMD();

                girisform.Show();


                this.Hide();


            }
            else if (comboislem.SelectedIndex == 1)
            {

                EkMsı girisform = new EkMsı();

                girisform.Show();

                this.Hide();
            }

        }

        private void İslemciForm_Load(object sender, EventArgs e)
        {
            comboislem.Items.Add("AMD");
            comboislem.Items.Add("İNTEL");
            comboislem.ForeColor = Color.DarkRed;
            comboislem.BackColor = Color.Gray;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void geriIslem_Click(object sender, EventArgs e)
        {
            DonanımEntities dnm = new DonanımEntities();

            Form2 girisform = new Form2();

            girisform.Show();

            this.Hide();
        }
    }
}
