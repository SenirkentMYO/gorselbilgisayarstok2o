﻿namespace bilgisyrStok
{
    partial class HarddSegateList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Hardsegatelist = new System.Windows.Forms.Button();
            this.Segategeri = new System.Windows.Forms.Button();
            this.artanSegate = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // Hardsegatelist
            // 
            this.Hardsegatelist.Location = new System.Drawing.Point(207, 164);
            this.Hardsegatelist.Name = "Hardsegatelist";
            this.Hardsegatelist.Size = new System.Drawing.Size(62, 23);
            this.Hardsegatelist.TabIndex = 0;
            this.Hardsegatelist.Text = "Listele";
            this.Hardsegatelist.UseVisualStyleBackColor = true;
            this.Hardsegatelist.Click += new System.EventHandler(this.Hardsegatelist_Click);
            // 
            // Segategeri
            // 
            this.Segategeri.Location = new System.Drawing.Point(12, 164);
            this.Segategeri.Name = "Segategeri";
            this.Segategeri.Size = new System.Drawing.Size(67, 23);
            this.Segategeri.TabIndex = 1;
            this.Segategeri.Text = "Geri";
            this.Segategeri.UseVisualStyleBackColor = true;
            this.Segategeri.Click += new System.EventHandler(this.button2_Click);
            // 
            // artanSegate
            // 
            this.artanSegate.Location = new System.Drawing.Point(207, 193);
            this.artanSegate.Name = "artanSegate";
            this.artanSegate.Size = new System.Drawing.Size(65, 23);
            this.artanSegate.TabIndex = 2;
            this.artanSegate.Text = "Artan Fiyat";
            this.artanSegate.UseVisualStyleBackColor = true;
            this.artanSegate.Click += new System.EventHandler(this.button3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(260, 156);
            this.dataGridView1.TabIndex = 3;
            // 
            // HarddSegateList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.artanSegate);
            this.Controls.Add(this.Segategeri);
            this.Controls.Add(this.Hardsegatelist);
            this.Name = "HarddSegateList";
            this.Text = "HarddSegateList";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Hardsegatelist;
        private System.Windows.Forms.Button Segategeri;
        private System.Windows.Forms.Button artanSegate;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}