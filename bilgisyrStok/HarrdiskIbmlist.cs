﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class HarrdiskIbmlist : Form
    {
        public HarrdiskIbmlist()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<WvHardDisk> don = new List<WvHardDisk>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvHardDisk.Where(a => a.HddMarka.Contains("IBM")).ToList();

            dataIBM.DataSource = don;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();

            HarddForm goster = new HarddForm();
            goster.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            List<WvHardDisk> listele = db.WvHardDisk.OrderBy(a => a.HddMarka).Where(a => a.HddMarka.Contains("IBM")).ToList();
            dataIBM.DataSource = listele;
        }
    }
}
