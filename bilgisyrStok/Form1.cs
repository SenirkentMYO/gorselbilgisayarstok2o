﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<Kullanici> lis = new List<Kullanici>();
            DonanımEntities dnm = new DonanımEntities();
            lis = dnm.Kullanici.Where(b => b.Email == txt1.Text && b.Sifre == txt2.Text).ToList(); 
            
            if (lis.Count == 1)
            {
                Form2 girisform = new Form2();

                girisform.Show();
                
                this.Hide();
            }
            else
            {
                MessageBox.Show("Yanlış Girdiniz !");

            }
           
        }

        private void txt2_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}
