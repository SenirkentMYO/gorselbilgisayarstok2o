﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class İslemAMD : Form
    {
        public İslemAMD()
        {
            InitializeComponent();
        }

        private void ListeAmd_Click(object sender, EventArgs e)
        {
            List<Wvİslemci> don = new List<Wvİslemci>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.Wvİslemci.Where(a => a.İslmMarka.Contains("AMD")).ToList();

           dataAmd.DataSource = don;
        }
    }
}
