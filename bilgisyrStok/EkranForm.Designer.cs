﻿namespace bilgisyrStok
{
    partial class EkranForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboekran = new System.Windows.Forms.ComboBox();
            this.lblekran = new System.Windows.Forms.Label();
            this.btnEkran = new System.Windows.Forms.Button();
            this.geriparca = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboekran
            // 
            this.comboekran.FormattingEnabled = true;
            this.comboekran.Location = new System.Drawing.Point(133, 74);
            this.comboekran.Name = "comboekran";
            this.comboekran.Size = new System.Drawing.Size(121, 21);
            this.comboekran.TabIndex = 0;
            // 
            // lblekran
            // 
            this.lblekran.AutoSize = true;
            this.lblekran.ForeColor = System.Drawing.Color.Coral;
            this.lblekran.Location = new System.Drawing.Point(59, 74);
            this.lblekran.Name = "lblekran";
            this.lblekran.Size = new System.Drawing.Size(57, 13);
            this.lblekran.TabIndex = 1;
            this.lblekran.Text = "Markalar =";
            // 
            // btnEkran
            // 
            this.btnEkran.Location = new System.Drawing.Point(179, 120);
            this.btnEkran.Name = "btnEkran";
            this.btnEkran.Size = new System.Drawing.Size(75, 23);
            this.btnEkran.TabIndex = 2;
            this.btnEkran.Text = "Seç";
            this.btnEkran.UseVisualStyleBackColor = true;
            this.btnEkran.Click += new System.EventHandler(this.btnEkran_Click);
            // 
            // geriparca
            // 
            this.geriparca.Location = new System.Drawing.Point(25, 120);
            this.geriparca.Name = "geriparca";
            this.geriparca.Size = new System.Drawing.Size(75, 23);
            this.geriparca.TabIndex = 3;
            this.geriparca.Text = "Geri";
            this.geriparca.UseVisualStyleBackColor = true;
            this.geriparca.Click += new System.EventHandler(this.geriparca_Click);
            // 
            // EkranForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.geriparca);
            this.Controls.Add(this.btnEkran);
            this.Controls.Add(this.lblekran);
            this.Controls.Add(this.comboekran);
            this.Name = "EkranForm";
            this.Text = "EkranKartı";
            this.Load += new System.EventHandler(this.EkranForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboekran;
        private System.Windows.Forms.Label lblekran;
        private System.Windows.Forms.Button btnEkran;
        private System.Windows.Forms.Button geriparca;
    }
}