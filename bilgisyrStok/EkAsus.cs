﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class EkAsus : Form
    {
        public EkAsus()
        {
            InitializeComponent();
        }

        private void dataw_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btnasus_Click(object sender, EventArgs e)
        {
            List<WvEkranKarti> don = new List<WvEkranKarti>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvEkranKarti.Where(a => a.EKartMarka.Contains("ASUS")).ToList();
            
            dataw.DataSource = don;
        }

        private void artanEkasus_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            List<WvEkranKarti> listele = db.WvEkranKarti.OrderBy(a => a.EKartFiyat).Where(a => a.EKartMarka.Contains("ASUS")).ToList();

            dataw.DataSource = listele;
        }

        private void Ekasusgeri_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();

            EkranForm goster = new EkranForm();
            goster.Show();
            this.Hide();
        }
    }
}
