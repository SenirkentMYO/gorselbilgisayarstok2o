﻿namespace bilgisyrStok
{
    partial class AnAsusList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.gerianakform = new System.Windows.Forms.Button();
            this.anakAsusArtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(-1, -2);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(286, 157);
            this.dataGridView1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Listele";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // gerianakform
            // 
            this.gerianakform.Location = new System.Drawing.Point(27, 172);
            this.gerianakform.Name = "gerianakform";
            this.gerianakform.Size = new System.Drawing.Size(75, 23);
            this.gerianakform.TabIndex = 2;
            this.gerianakform.Text = "Geri";
            this.gerianakform.UseVisualStyleBackColor = true;
            this.gerianakform.Click += new System.EventHandler(this.gerianakform_Click);
            // 
            // anakAsusArtn
            // 
            this.anakAsusArtn.Location = new System.Drawing.Point(197, 216);
            this.anakAsusArtn.Name = "anakAsusArtn";
            this.anakAsusArtn.Size = new System.Drawing.Size(75, 23);
            this.anakAsusArtn.TabIndex = 3;
            this.anakAsusArtn.Text = "Artan Fiyat";
            this.anakAsusArtn.UseVisualStyleBackColor = true;
            this.anakAsusArtn.Click += new System.EventHandler(this.anakAsusArtn_Click);
            // 
            // AnAsusList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.anakAsusArtn);
            this.Controls.Add(this.gerianakform);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AnAsusList";
            this.Text = "AnAsusList";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button gerianakform;
        private System.Windows.Forms.Button anakAsusArtn;
    }
}