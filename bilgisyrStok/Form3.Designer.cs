﻿namespace bilgisyrStok
{
    partial class Anakform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbomrk = new System.Windows.Forms.ComboBox();
            this.lbl5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.geri = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbomrk
            // 
            this.cmbomrk.FormattingEnabled = true;
            this.cmbomrk.Location = new System.Drawing.Point(89, 50);
            this.cmbomrk.Name = "cmbomrk";
            this.cmbomrk.Size = new System.Drawing.Size(163, 21);
            this.cmbomrk.TabIndex = 0;
            this.cmbomrk.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.BackColor = System.Drawing.Color.Black;
            this.lbl5.ForeColor = System.Drawing.Color.Coral;
            this.lbl5.Location = new System.Drawing.Point(27, 53);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(54, 13);
            this.lbl5.TabIndex = 1;
            this.lbl5.Text = "Markalar :";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Seç";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // geri
            // 
            this.geri.Location = new System.Drawing.Point(37, 94);
            this.geri.Name = "geri";
            this.geri.Size = new System.Drawing.Size(75, 23);
            this.geri.TabIndex = 3;
            this.geri.Text = "Geri";
            this.geri.UseVisualStyleBackColor = true;
            this.geri.Click += new System.EventHandler(this.geri_Click);
            // 
            // Anakform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.geri);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl5);
            this.Controls.Add(this.cmbomrk);
            this.Name = "Anakform";
            this.Text = "AnaKart";
            this.Load += new System.EventHandler(this.Form3_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbomrk;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button geri;
    }
}