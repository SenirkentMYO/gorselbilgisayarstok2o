﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace bilgisyrStok
{
    public partial class AnAsusList : Form
    {
        public AnAsusList()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            List<WvAnaKart> don = new List<WvAnaKart>();
            DonanımEntities donentiti = new DonanımEntities();
            don = donentiti.WvAnaKart.Where(a => a.AnaKMarka.Contains("ASUS")).ToList();

            dataGridView1.DataSource = don;
        }

        private void gerianakform_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            Anakform goster = new Anakform();
            goster.Show();
            this.Hide();
        }

        private void anakAsusArtn_Click(object sender, EventArgs e)
        {
            DonanımEntities db = new DonanımEntities();
            List<WvAnaKart> listele = db.WvAnaKart.OrderBy(a => a.AnaKFiyati).Where(a => a.AnaKMarka.Contains("Asus")).ToList();
            dataGridView1.DataSource = listele;

        }
    }
}
